import Image from "next/image";
import React from "react";

const Comments = () => {
  return (
    <div>
      <div className="  mb-4 border-t ">
        <h2> Comments</h2>
        <div className="  px-6 py-4 border-b  ">
          <div className="flex   items-center justify-between">
            <div className="flex items-center     mb-6">
              <Image
                src="https://randomuser.me/api/portraits/men/97.jpg"
                alt="Avatar"
                className="w-12 h-12 
        rounded-full mr-4"
                height={50}
                width={50}
              />
              <div>
                <h2 className="text-lg font-medium ">John Doe</h2>
                <h2>2 hours ago</h2>
              </div>
            </div>

            <h2 className="flex  ">
              <svg
                className="h-6 w-6"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                {" "}
                <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z" />
              </svg>
              <div className="w-[40px]  text-xl ">2k</div>
            </h2>
          </div>
          <p className="text-lg leading-relaxed mb-6">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit
            amet lorem nulla. Donec consequat urna a tortor sagittis lobortis.
          </p>
          <h2 className="flex justify-between items-center">
            <a href="#">
              <i className="far fa-comment-alt"></i> Reply
            </a>
          </h2>
        </div>
      </div>
      <div
        className="relative flex s-center self-center
               w-full p-4 overflow-hidden text-gray-600
                focus-within:text-gray-400"
      >
        <Image
          className="w-10 h-10 object-cover rounded-full shadow mr-2 cursor-pointer"
          alt="User avatar"
          src="https://images.unsplash.com/photo-1477118476589-bff2c5c4cfbb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=200&q=200"
          width={40}
          height={50}
        />
        <span className="absolute inset-y-0 right-0 flex s-center pr-6">
          <button
            type="submit"
            className="p-1 focus:outline-none focus:shadow-none hover:text-blue-500"
          >
            <svg
              className="w-6 h-6 transition ease-out duration-300 hover:text-blue-500 text-gray-400"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M14.828 14.828a4 4 0 01-5.656 0M9 10h.01M15 10h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
              />
            </svg>
          </button>
        </span>
        <input
          type="search"
          className="w-full py-2 pl-4 pr-10 text-sm border appearance-none rounded-tg
                  dark:bg-[#ffffff] focus:outline-none focus:shadow-outline-blue"
          style={{ borderRadius: 25 }}
          placeholder="Post a comment..."
        />
      </div>
    </div>
  );
};

export default Comments;
