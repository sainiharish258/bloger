"use client";
import { Item } from "@/app/api/newblog/route";
import Image from "next/image";
import React, { useState } from "react";
import Comments from "./Comments";
import Link from "next/link";
import { NextResponse } from "next/server";
import dbconnect from "@/lib/dbconnect/dbconnect";
export interface IdpageProps {
  item: Item;
}

const Idpage = ({ item }: IdpageProps) => {
  if (typeof item.Likes === "string") {
    item.Likes = Number(item.Likes);
  }
  const [likebtnfill, setLikebtnFill] = useState("none");
  const [likebtnclour, setLikebtnClour] = useState("currentColor");
  const [likeCounter, setLikeCounter] = useState<number>(item.Likes); // Correctly typed as number
  const [isLiked, setIsLiked] = useState(false);

  const UpdateLike = async () => {
    if (isLiked) {
      setLikebtnFill("none");
      setLikebtnClour("currentColor");
      setLikeCounter((prevCounter) => {
        const newCounter = prevCounter - 1;
        updateLikes(newCounter);
        return newCounter;
      });
    } else {
      setLikebtnFill("red");
      setLikebtnClour("red");
      setLikeCounter((prevCounter) => {
        const newCounter = prevCounter + 1;
        updateLikes(newCounter);
        return newCounter;
      });
    }
    setIsLiked(!isLiked);
  };

  const updateLikes = async (newLikes: Number) => {
    try {
      const response = await fetch(`/api/newblog?id=${item.urlkey}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          urlkey: item.urlkey,
          Likes: newLikes,
        }),
      });

      const data = await response.json();

      if (!response.ok) {
        throw new Error(data.msg || "Something went wrong");
      }

      console.log({
        msg: "PUT Request Successfully",
        data: data,
      });
    } catch (error) {
      console.error("Error updating likes:", error);
    }
  };

  return (
    <>
      <div className="sm:m-21 p-3 mt-20 mb-20 flex justify-center">
        <div className="flex shadow-md rounded-lg overflow-hidden mx-auto">
          <div className="flex s-center w-full">
            <div className="w-full">
              <div
                className="relative pb-[60%] md:pb-[25%] lg:pb-[25%] overflow-hidden 
               xl:-translate-y-4 group-hover:translate-x-0 group-hover:translate-y-0 transition duration-700
                ease-out"
              >
                <Image
                  className="absolute inset-0 w-full !h-full object-cover transform 
                     hover:scale-105 transition duration-700 ease-out"
                  src={item.Image} // Ensure 'item' has an 'image' property
                  width={400}
                  height={50}
                  alt="Blog post"
                />
              </div>
              <p className="font-semibold text-lg mt-5 mx-3 px-2">
                {item.title}
              </p>
              <p className="mb-6 text-lg mx-3 px-2"> {item.article}</p>

              <div className="flex justify-start mb-4 border-t">
                <div className="flex mt-1 pt-2 pl-5">
                  <h3 className="flex pr-2 text-xl">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      width="30px"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M8 10h.01M12 10h.01M16 10h.01M21 12c0-4.418-4.03-8-9-8S3 7.582 3 12c0 3.867 3.134 7.043 7.2 7.75a.75.75 0 01.6.75v2.25a.75.75 0 001.2.6l2.9-2.32a.75.75 0 01.6-.18c4.07-.75 7.5-3.92 7.5-7.85z"
                      />
                    </svg>
                  </h3>

                  <Image
                    className="inline-block object-cover w-8 h-8 border-2 border-white rounded-full shadow-sm cursor-pointer"
                    src="https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                    alt=""
                    width={40}
                    height={50}
                  />
                  <Image
                    className="inline-block object-cover w-8 h-8 -ml-2 border-2 border-white rounded-full shadow-sm cursor-pointer"
                    src="https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                    alt=""
                    width={40}
                    height={50}
                  />
                  <Image
                    className="inline-block object-cover w-8 h-8 -ml-2 border-2 border-white rounded-full shadow-sm cursor-pointer"
                    src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80"
                    alt=""
                    width={40}
                    height={50}
                  />
                  <Image
                    className="inline-block object-cover w-8 h-8 -ml-2 border-2 border-white rounded-full shadow-sm cursor-pointer"
                    src="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.25&w=256&h=256&q=80"
                    alt=""
                    width={40}
                    height={50}
                  />
                  <h3 className="pt-1 text-xl pl-2 ">
                    {item.Comments.length}+
                  </h3>
                </div>
                <div className="flex justify-end w-full mt-1 pt-2 pr-5">
                  <span
                    className="transition ease-out duration-300 hover:bg-blue-50 bg-blue-100 h-8 px-2 py-2 text-center rounded-full
                   text-blue-400 cursor-pointer mr-2"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      width="14px"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M8.684 13.342C8.886 12.938 9 12.482 9 12c0-.482-.114-.938-.316-1.342m0 2.684a3 3 0 110-2.684m0 2.684l6.632 3.316m-6.632-6l6.632-3.316m0 0a3 3 0 105.367-2.684 3 3 0 00-5.367 2.684zm0 9.316a3 3 0 105.368 2.684 3 3 0 00-5.368-2.684z"
                      />
                    </svg>
                  </span>
                  <span
                    className="transition ease-out duration-300 hover:bg-blue-500 bg-blue-600 h-8 px-2 py-2
                   text-center rounded-full cursor-pointer"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      width="14px"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"
                      />
                    </svg>
                  </span>
                </div>
              </div>

              <div className=" flex justify-between  items-center border-b">
                <div className="mt-3 mx-5 flex-col flex">
                  <h3 className=" text-sm">Views:{item.Views}</h3>
                </div>
                <div onClick={UpdateLike} className="mt-3 mr-9">
                  <h3 className=" flex flex-col justify-center items-center">
                    <svg
                      className="h-6 w-6"
                      viewBox="0 0 24 24"
                      fill={likebtnfill}
                      stroke={likebtnclour}
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    >
                      {" "}
                      <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z" />
                    </svg>
                    <div className=" text-xl">{likeCounter}</div>
                  </h3>
                </div>
              </div>

              {/*    <Comments /> */}
              <Comments />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Idpage;
