export const newdeta = [
  {
    Image: `https://picsum.photos/536/354`,
    Category: "photos",
    uploadTime: "19-5-2024",
    title: "Designing a functional workflow at home",
    description:
      "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat",
    article: `1. Set Up Your Workspace
    Dedicated Space: Designate a specific area in your home for work. Ideally, this should be a quiet, well-lit space with minimal distractions.
    Ergonomic Setup: Invest in a comfortable chair, desk, and monitor setup to promote good posture and reduce physical strain.
    Personalization: Make the space your own with plants, art, or decor that inspires creativity and focus.
    2. Establish a Routine
    Set Regular Hours: Define your work hours and stick to them consistently to establish a sense of structure.
    Morning Ritual: Start your day with a routine that signals the transition into work mode, such as meditation, exercise, or a morning walk.
    3. Use Task Management Tools
    Digital Tools: Utilize task management apps like Todoist, Trello, or Asana to organize your workload and prioritize tasks.
    Time Blocking: Allocate specific time slots for different tasks throughout the day to maintain focus and manage time effectively.
  `,
    Comments:
      "  Time Blocking: Allocate specific time slots for different tasks throughout the day to maintain focus and manage time effectively",
    Views: "10k",
    Likes: "2k",
    save: "",
    share: "",
    downlode: "",
  },
  {
    Image: `https://picsum.photos/537/354`,
    Category: "photos",
    uploadTime: "19-5-2024",
    title: "Designing a functional workflow at home",
    description:
      "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat",
    article: `1. Set Up Your Workspace
    Dedicated Space: Designate a specific area in your home for work. Ideally, this should be a quiet, well-lit space with minimal distractions.
    Ergonomic Setup: Invest in a comfortable chair, desk, and monitor setup to promote good posture and reduce physical strain.
    Personalization: Make the space your own with plants, art, or decor that inspires creativity and focus.
    2. Establish a Routine
    Set Regular Hours: Define your work hours and stick to them consistently to establish a sense of structure.
    Morning Ritual: Start your day with a routine that signals the transition into work mode, such as meditation, exercise, or a morning walk.
    3. Use Task Management Tools
    Digital Tools: Utilize task management apps like Todoist, Trello, or Asana to organize your workload and prioritize tasks.
    Time Blocking: Allocate specific time slots for different tasks throughout the day to maintain focus and manage time effectively.
  `,
    Comments:
      "  Time Blocking: Allocate specific time slots for different tasks throughout the day to maintain focus and manage time effectively",
    Views: "10k",
    Likes: "2k",
    save: "",
    share: "",
    downlode: "",
  },
  {
    Image: `https://picsum.photos/535/354`,
    Category: "photos",
    uploadTime: "19-5-2024",
    title: "Designing a functional workflow at home",
    description:
      "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat",
    article: `1. Set Up Your Workspace
    Dedicated Space: Designate a specific area in your home for work. Ideally, this should be a quiet, well-lit space with minimal distractions.
    Ergonomic Setup: Invest in a comfortable chair, desk, and monitor setup to promote good posture and reduce physical strain.
    Personalization: Make the space your own with plants, art, or decor that inspires creativity and focus.
    2. Establish a Routine
    Set Regular Hours: Define your work hours and stick to them consistently to establish a sense of structure.
    Morning Ritual: Start your day with a routine that signals the transition into work mode, such as meditation, exercise, or a morning walk.
    3. Use Task Management Tools
    Digital Tools: Utilize task management apps like Todoist, Trello, or Asana to organize your workload and prioritize tasks.
    Time Blocking: Allocate specific time slots for different tasks throughout the day to maintain focus and manage time effectively.
  `,
    Comments:
      "  Time Blocking: Allocate specific time slots for different tasks throughout the day to maintain focus and manage time effectively",
    Views: "10k",
    Likes: "2k",
    save: "",
    share: "",
    downlode: "",
  },
  {
    Image: `https://picsum.photos/534/354`,
    Category: "photos",
    uploadTime: "19-5-2024",
    title: "Designing a functional workflow at home",
    description:
      "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat",
    article: `1. Set Up Your Workspace
    Dedicated Space: Designate a specific area in your home for work. Ideally, this should be a quiet, well-lit space with minimal distractions.
    Ergonomic Setup: Invest in a comfortable chair, desk, and monitor setup to promote good posture and reduce physical strain.
    Personalization: Make the space your own with plants, art, or decor that inspires creativity and focus.
    2. Establish a Routine
    Set Regular Hours: Define your work hours and stick to them consistently to establish a sense of structure.
    Morning Ritual: Start your day with a routine that signals the transition into work mode, such as meditation, exercise, or a morning walk.
    3. Use Task Management Tools
    Digital Tools: Utilize task management apps like Todoist, Trello, or Asana to organize your workload and prioritize tasks.
    Time Blocking: Allocate specific time slots for different tasks throughout the day to maintain focus and manage time effectively.
  `,
    Comments:
      "  Time Blocking: Allocate specific time slots for different tasks throughout the day to maintain focus and manage time effectively",
    Views: "10k",
    Likes: "2k",
    save: "",
    share: "",
    downlode: "",
  },
  {
    Image: `https://picsum.photos/533/354`,
    Category: "photos",
    uploadTime: "19-5-2024",
    title: "Designing a functional workflow at home",
    description:
      "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat",
    article: `1. Set Up Your Workspace
    Dedicated Space: Designate a specific area in your home for work. Ideally, this should be a quiet, well-lit space with minimal distractions.
    Ergonomic Setup: Invest in a comfortable chair, desk, and monitor setup to promote good posture and reduce physical strain.
    Personalization: Make the space your own with plants, art, or decor that inspires creativity and focus.
    2. Establish a Routine
    Set Regular Hours: Define your work hours and stick to them consistently to establish a sense of structure.
    Morning Ritual: Start your day with a routine that signals the transition into work mode, such as meditation, exercise, or a morning walk.
    3. Use Task Management Tools
    Digital Tools: Utilize task management apps like Todoist, Trello, or Asana to organize your workload and prioritize tasks.
    Time Blocking: Allocate specific time slots for different tasks throughout the day to maintain focus and manage time effectively.
  `,
    Comments:
      "  Time Blocking: Allocate specific time slots for different tasks throughout the day to maintain focus and manage time effectively",
    Views: "10k",
    Likes: "2k",
    save: "",
    share: "",
    downlode: "",
  },
];
