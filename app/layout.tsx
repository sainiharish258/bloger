import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { Providers } from "./providers";
import ThemeSwitch from "./components/ThemeSwitch";
import Navbar from "./components/Navbar";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "San Raj",
  description: "A place where dreams are digitised.",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" suppressHydrationWarning>
      <body
        className={`${inter.className} dark:text-red-50
       text-[#232323]`}
      >
        <Providers>
          <ThemeSwitch />
          <Navbar />
          {children}
        </Providers>
      </body>
    </html>
  );
}
