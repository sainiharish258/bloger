import SkeletonPlaceholder from "@/app/Dammydeta";
import Fullblog from "@/app/components/Fullblog";
import Custom404 from "@/app/not-found";
import NotFound from "@/app/not-found";
import { Metadata, ResolvingMetadata } from "next";
import Head from "next/head";
import { headers } from "next/headers";
import Link from "next/link";

export async function generateMetadata({ params }: any): Promise<Metadata> {
  const header = headers();
  const scheme = header.get("x-forwarded-proto");
  const host = header.get("x-forwarded-host");
  const response = await fetch(
    `${scheme}://${host}/api/newblog?id=${params.id}`
  );
  if (!response.ok) {
    throw new Error("Failed to fetch data");
  }

  const responseData = await response.json();

  if (!responseData.data) return {};

  return {
    title: `${responseData.data.title} | San Raj`,
    openGraph: {
      images: [responseData.data.image],
    },
  };
}

const Idpage = async ({ params }: any) => {
  const header = headers();
  const fetchData = async () => {
    try {
      const scheme = header.get("x-forwarded-proto");
      const host = header.get("x-forwarded-host");
      const response = await fetch(
        `${scheme}://${host}/api/newblog?id=${params.id}`
      );
      if (!response.ok) {
        throw new Error("Failed to fetch data");
      }

      const responseData = await response.json();
      const data = responseData.data; // Assuming your data is nested under a 'data' key

      return data || null;
    } catch (error) {
      console.error("Error fetching data:", error);
      return null;
    }
  };

  const blogItem = await fetchData();

  if (!blogItem)
    return (
      <>
        <Custom404 />
      </>
    );

  return (
    <div>
      <Fullblog item={blogItem} />
    </div>
  );
};

export default Idpage;
