import dbconnect from "@/lib/dbconnect/dbconnect";
import newblog from "../../../lib/models/Neblogdetamodel";
import mongoose from "mongoose";
import { NextRequest, NextResponse } from "next/server";

export const PUT = async (
  req: NextRequest,
  { params }: { params: { Blogid: string } }
) => {
  const blogid = params.Blogid;

  // Ensure the ID is a valid ObjectId
  if (!mongoose.Types.ObjectId.isValid(blogid)) {
    return NextResponse.json({ message: "Invalid blog ID" }, { status: 400 });
  }

  const id = { _id: new mongoose.Types.ObjectId(blogid) };

  try {
    const payload = await req.json();
    console.log(payload);

    // Connect to the database
    await dbconnect();

    // Update the blog post and return the updated document
    const result = await newblog.findOneAndUpdate(id, payload, { new: true });

    if (!result) {
      return NextResponse.json({ message: "Blog not found" }, { status: 404 });
    }

    return NextResponse.json({ result });
  } catch (error: any) {
    console.error(error);
    return NextResponse.json(
      { message: "An error occurred", error: error.message },
      { status: 500 }
    );
  }
};
