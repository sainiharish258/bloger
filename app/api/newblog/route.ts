import dbconnect from "@/lib/dbconnect/dbconnect";
import { Schema, model, models } from "mongoose";
import { NextRequest, NextResponse } from "next/server";

// Define the Mongoose schema
const DataModelSchema = new Schema({
  Image: String,
  Category: String,
  uploadTime: String,
  title: String,
  description: String,
  article: String,
  profilName: String,
  Comments: String,
  Views: String,
  Likes: Number, // Ensure this is defined as a Number
  disLikes: String,
  saveData: String, // renamed to avoid conflict with reserved keyword
  share: String,
  downlode: String,
  urlkey: { type: String, unique: true },
});

// Initialize the model only if it hasn't been initialized already
const DataModel = models.DataModel || model("DataModel", DataModelSchema);

// Define the interface for the item structure
export interface Item {
  Image: string;
  Category: string;
  uploadTime: string;
  title: string;
  description: string;
  article: string;
  profilName: string;
  Comments: string;
  Views: string;
  Likes: number; // Ensure this is defined as a number
  disLikes: string;
  saveData: string; // renamed to avoid conflict with reserved keyword
  share: string;
  downlode: string;
  urlkey: string;
}

// Define the interface for the data object with dynamic keys
export interface DataModel {
  [urlkey: string]: Item;
}

export async function GET(req: NextRequest) {
  try {
    await dbconnect();

    const urlkey = req.nextUrl.searchParams.get("id");
    if (urlkey) {
      const data: Item | null = await DataModel.findOne({ urlkey }).exec();
      if (data) {
        return NextResponse.json({ data });
      } else {
        return NextResponse.json({ msg: "Data not found" });
      }
    }

    const data: Item[] = await DataModel.find().exec();

    // Transform the array into an object with urlkey as the key
    const dataObject: DataModel = {};
    data.forEach((item) => {
      dataObject[item.urlkey] = item;
    });

    return NextResponse.json({ data: dataObject });
  } catch (error) {
    return NextResponse.json({
      msg: "Something went wrong in the GET request",
    });
  }
}

export async function POST(req: any) {
  try {
    const body = await req.json();

    // Validate required fields
    if (!body.urlkey || !body.title) {
      return NextResponse.json({ msg: "Missing required fields" });
    }

    await dbconnect();
    const newData = new DataModel({
      urlkey: body.urlkey,
      Image: body.Image,
      Category: body.Category,
      uploadTime: body.uploadTime,
      title: body.title,
      description: body.description,
      article: body.article,
      profilName: body.profilName,
      Comments: body.Comments,
      Views: body.Views,
      Likes: body.Likes, // Ensure this is a number in your POST request
      disLikes: body.disLikes,
      saveData: body.saveData, // renamed to avoid conflict with reserved keyword
      share: body.share,
      downlode: body.downlode,
    });

    await newData.save();
    return NextResponse.json({ msg: "POST Request Successfully" });
  } catch (error) {
    console.error("An error occurred in the POST request:", error);
    return NextResponse.json({ msg: "An error occurred in the POST request" });
  }
}

export async function PUT(req: any) {
  try {
    const body = await req.json();

    // Validate required fields
    if (!body.urlkey) {
      return NextResponse.json({ msg: "Missing required urlkey" });
    }

    await dbconnect();
    const updatedData = await DataModel.findOneAndUpdate(
      { urlkey: body.urlkey },
      body,
      { new: true }
    ).exec();

    if (!updatedData) {
      return NextResponse.json({
        msg: "No document found with the provided urlkey",
      });
    }

    return NextResponse.json({
      msg: "PUT Request Successfully",
      data: updatedData,
    });
  } catch (error) {
    console.error("An error occurred in the PUT request:", error);
    return NextResponse.json({ msg: "An error occurred in the PUT request" });
  }
}

export async function DELETE(req: NextRequest) {
  try {
    const urlkey = req.nextUrl.searchParams.get("id");

    if (!urlkey) {
      return NextResponse.json({ msg: "Missing required urlkey" });
    }

    await dbconnect();
    await DataModel.findOneAndDelete({ urlkey }).exec();

    return NextResponse.json({ msg: "DELETE Request Successfully" });
  } catch (error) {
    console.error("An error occurred in the DELETE request:", error);
    return NextResponse.json({
      msg: "An error occurred in the DELETE request",
    });
  }
}
