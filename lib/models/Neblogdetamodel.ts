import mongoose from "mongoose";

const newblogSchema = new mongoose.Schema({
  urlkey: { type: String },
  Image: { type: String },
  Category: { type: String },
  uploadTime: { type: String },
  title: { type: String },
  description: { type: String },
  article: { type: String },
  profilName: { type: String },
  Comments: { type: String },
  Views: { type: String },
  Likes: { type: Number },
  disLikes: { type: String },
  save: { type: String },
  share: { type: String },
  downlode: { type: String },
});

const DataModel =
  mongoose.models.newblog || mongoose.model("newblog", newblogSchema);

export default DataModel;
